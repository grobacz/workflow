<?php

namespace App\Command;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class Transition extends Command
{
    protected static $defaultName = "app:transition";
    
    private MessageBusInterface $dispatcher;
    private EntityManagerInterface $entityManager;
    private WorkflowInterface $workflow;
    
    public function __construct(
        MessageBusInterface $dispatcher,
        EntityManagerInterface $entityManager,
        WorkflowInterface $workflow
    ) {
        parent::__construct();
        $this->dispatcher = $dispatcher;
        $this->entityManager = $entityManager;
        $this->workflow = $workflow;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Event $bla */
        $bla = $this->entityManager->getRepository(Event::class)->find(1);
        $this->entityManager->beginTransaction();
        if ($bla->getStatus() == 'init') {
            $this->workflow->apply($bla, 'doit');
        } else {
            $this->workflow->apply($bla, 'dont');
        }
        $this->dispatcher->dispatch($bla);
        sleep(2);
        $this->entityManager->persist($bla);
        $this->entityManager->flush();
        $this->entityManager->commit();
        return 0;
    }

}