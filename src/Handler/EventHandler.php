<?php

namespace App\Handler;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EventHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Event $event)
    {
        echo "EventHandler!" . PHP_EOL;
        $e = $this->entityManager->getRepository(Event::class)->find($event->getId());
        var_dump($e);
    }

//    public static function getSubscribedEvents()
//    {
//        return [Event::class => 'onEvent'];
//    }
}